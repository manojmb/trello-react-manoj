import React, { useState } from "react";
import { CardActions } from "@mui/material";

import CircularLoading from "./CircularLoading";
import { getAllCards, createCard, deleteCardById } from "../api/API";
import CardModal from "./CardModal";
import AddItemWithForm from "./AddItemWithForm";
import { Add as AddIcon, Close as CloseIcon } from "@mui/icons-material";
import useApi from "../hooks/useApi";

import {
  ListCardContent,
  CardContainer,
  AddCardButtonStyle,
} from "../styles/MaterialStyles";
import ToastError from "./ToastError";

const card = [
  AddCardButtonStyle,
  <>
    <AddIcon style={{ fill: "#FFF" }} />
    Add a card
  </>,
  "Enter a title for this card...",
  <CloseIcon style={{ fill: "#fff" }} />,
];

function Cards({ listId }) {
  const [toastError, setToastError] = useState("");
  const {
    data: cardData,
    setData: setCardData,
    error,
    isLoading,
  } = useApi(getAllCards, listId);

  async function createCardByName(itemName) {
    try {
      const response = await createCard(listId, itemName);
      setCardData((prevData) => [...prevData, response.data]);
    } catch (error) {
      setToastError(error.message);
    }
  }

  return (
    <>
      {toastError && <ToastError message={toastError} />}
      {error && (
        <p style={{ color: "white", textAlign: "center" }}>
          {error.message === "Network Error"
            ? error.message
            : "Error fetching cards"}
        </p>
      )}
      {isLoading && <CircularLoading />}
      {!error && !isLoading && (
        <CardContainer>
          {cardData.map((card) => (
            <ListCardContent key={card.id}>
              <CardModal card={card} setCardData={setCardData} />
            </ListCardContent>
          ))}
        </CardContainer>
      )}

      <CardActions>
        <AddItemWithForm createItem={createCardByName} itemProperties={card} />
      </CardActions>
    </>
  );
}

export default Cards;

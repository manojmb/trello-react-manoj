import React, { useState } from "react";
import { Button } from "@mui/material";

import ToastError from "./ToastError";

import {
  StyledTextField,
  ListCard,
  AddCardForm,
} from "../styles/MaterialStyles";

function AddItemWithForm({ itemProperties, createItem }) {
  const [inputItemName, setInputItemName] = useState("");
  const [addItemState, setAddItemState] = useState(false);

  const [error, setError] = useState("");

  const addItem = async (e) => {
    e.preventDefault();
    try {
      createItem(inputItemName);
      setError("");
    } catch (error) {
      setError(error.message);
    }

    setAddItemState(false);
    setInputItemName("");
  };

  return (
    <>
      {error && <ToastError message={error} />}
      <>
        {addItemState ? (
          <ListCard sx={{ backgroundColor: "inherit", boxShadow: "none" }}>
            <AddCardForm onSubmit={addItem}>
              <StyledTextField
                placeholder={itemProperties[2]}
                maxRows={1}
                value={inputItemName}
                onChange={(e) => setInputItemName(e.target.value)}
              />
              <div>
                <Button
                  size="small"
                  variant="contained"
                  type="submit"
                  sx={{ color: "#1D2125" }}
                >
                  Add
                </Button>
                <Button
                  size="small"
                  variant="text"
                  onClick={() => setAddItemState(false)}
                >
                  {itemProperties[3]}
                </Button>
              </div>
            </AddCardForm>
          </ListCard>
        ) : (
          <Button
            size="small"
            onClick={() => setAddItemState(true)}
            sx={itemProperties[0]}
          >
            {itemProperties[1]}
          </Button>
        )}
      </>
    </>
  );
}

export default AddItemWithForm;

import React, { useState } from "react";
import { Link } from "react-router-dom";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Typography, IconButton, Menu, MenuItem } from "@mui/material";

import { deleteBoardById } from "../api/API";
import CircularLoading from "./CircularLoading";
import ToastError from "./ToastError";

import { BoardCard, CardHeaderContainer } from "../styles/MaterialStyles";

import trello from "/trello1.jpg";

function Board({ boardId, boardName, setBoards }) {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  async function deleteBoard(id) {
    setIsLoading(true);
    try {
      await deleteBoardById(id);
      setBoards((prevData) => prevData.filter((board) => board.id !== id));
      setError("");
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }

    setAnchorEl(null);
  }

  return (
    <>
      {error && <ToastError message={error} />}
      {isLoading && <CircularLoading />}
      {!isLoading && (
        <BoardCard
          sx={{
            color: "white",
            background: `url(${trello})`,
            backgroundSize: "cover",
            alignItems: "flex-start",
          }}
        >
          <Link to={boardId} style={{ width: "100%" }}>
            <Typography
              variant="h6"
              style={{ fontWeight: 800, padding: "0.5rem" }}
            >
              {boardName}
            </Typography>
          </Link>

          <CardHeaderContainer
            action={
              <IconButton
                aria-label="settings"
                onClick={(event) => {
                  setAnchorEl(event.currentTarget);
                }}
              >
                <MoreVertIcon />
              </IconButton>
            }
          />
          <Menu
            anchorEl={anchorEl}
            open={open}
            onClose={() => setAnchorEl(null)}
          >
            <MenuItem onClick={() => deleteBoard(boardId)}>
              Delete Board
            </MenuItem>
          </Menu>
        </BoardCard>
      )}
    </>
  );
}

export default Board;

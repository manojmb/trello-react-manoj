import React, { useState } from "react";
import {
  FormGroup,
  FormControlLabel,
  Checkbox,
  LinearProgress,
  ListItem,
  Box,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

import {
  deleteCheckItemById,
  createCheckItems,
  getCheckItems,
  strikeCheckItems,
} from "../api/API";
import CircularLoading from "./CircularLoading";
import AddItemWithForm from "./AddItemWithForm";
import useApi from "../hooks/useApi";
import ToastError from "./ToastError";

const checkitem = [
  {
    backgroundColor: "#A1BDD914",
    padding: "0.2rem",
    color: "inherit",
  },
  "Add a item",
  "Add an item",
  "Cancel",
];

function CheckItems({ checkListId, cardId }) {
  const [toastError, setToastError] = useState("");
  const {
    data: chkItems,
    setData: setChkItems,
    error,
    isLoading,
  } = useApi(getCheckItems, checkListId);

  const progressValue =
    chkItems.length > 0
      ? Math.round(
          (chkItems.filter((chk) => chk.state === "complete").length * 100) /
            chkItems.length
        )
      : 0;

  async function createCheckItemsByName(itemName) {
    try {
      const response = await createCheckItems(checkListId, itemName);
      setChkItems((prevData) => [...prevData, response.data]);
    } catch (error) {
      throw error;
    }
  }

  async function deleteCheckItem(checkItemId) {
    try {
      await deleteCheckItemById(checkListId, checkItemId);
      setChkItems((prevData) =>
        prevData.filter((chkItem) => chkItem.id !== checkItemId)
      );
      setToastError("");
    } catch (error) {
      setToastError(error.message);
    }
  }

  const toggleCheckItem = async (checkItemId, state) => {
    state = state === "complete" ? "incomplete" : "complete";
    setChkItems((prevItems) => {
      return prevItems.map((item) => {
        if (item.id === checkItemId) {
          return { ...item, state: state };
        }
        return item;
      });
    });

    try {
      await strikeCheckItems(cardId, checkItemId, state);
      setToastError("");
    } catch (error) {
      setToastError(error.message);
    }
  };

  return (
    <>
      {toastError && <ToastError message={toastError} />}
      {error && (
        <p style={{ color: "white", textAlign: "center" }}>
          {error.message === "Network Error"
            ? error.message
            : "Error fetching checkitems"}
        </p>
      )}
      {isLoading && <CircularLoading />}
      {!error && !isLoading && (
        <>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <Typography
              variant="body2"
              color="white"
              style={{ padding: "0 0.5rem" }}
            >
              {progressValue}%
            </Typography>
            <Box sx={{ width: "100%", mr: 1 }}>
              <LinearProgress
                sx={
                  progressValue === 100
                    ? {
                        "& .MuiLinearProgress-bar": {
                          backgroundColor: "#4BCE97",
                        },
                      }
                    : {}
                }
                variant="determinate"
                value={progressValue}
              />
            </Box>
            <Box sx={{ minWidth: 35 }}></Box>
          </Box>

          {chkItems.map((chkItem) => (
            <ListItem key={chkItem.id} style={{ padding: "0 1rem" }}>
              <FormGroup
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  alignItems: "center",
                }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={chkItem.state === "complete" ? true : false}
                      onChange={() =>
                        toggleCheckItem(chkItem.id, chkItem.state)
                      }
                    />
                  }
                  label={chkItem.name}
                  sx={{
                    flex: 1,
                    textDecoration:
                      chkItem.state === "complete" ? "line-through" : "none",
                  }}
                />
                <DeleteIcon
                  sx={{ cursor: "pointer" }}
                  onClick={() => deleteCheckItem(chkItem.id)}
                />
              </FormGroup>
            </ListItem>
          ))}

          <AddItemWithForm
            itemProperties={checkitem}
            createItem={createCheckItemsByName}
          />
        </>
      )}
    </>
  );
}

export default CheckItems;

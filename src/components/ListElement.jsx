import React, { useState } from "react";
import { Menu, MenuItem, IconButton } from "@mui/material";
import { MoreHoriz as MoreHorizIcon } from "@mui/icons-material";

import CircularLoading from "./CircularLoading";
import { deleteListById } from "../api/API";
import Cards from "./Cards";
import ToastError from "./ToastError";

import { ListCard, ListHeader } from "../styles/MaterialStyles";

function ListElement({ listId, listName, setLists }) {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const deleteList = async (value) => {
    setIsLoading(true);
    try {
      await deleteListById(listId, value);
      setLists((prevData) => prevData.filter((list) => list.id != listId));
      setError("");
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      {error && <ToastError message={error} />}
      {isLoading && <CircularLoading />}
      {!isLoading && (
        <ListCard>
          <ListHeader
            action={
              <IconButton
                aria-label="settings"
                onClick={(event) => {
                  setAnchorEl(event.currentTarget);
                }}
              >
                <MoreHorizIcon style={{ fill: "white" }} />
              </IconButton>
            }
            titleTypographyProps={{ variant: "p" }}
            title={listName}
          />
          <Menu
            anchorEl={anchorEl}
            open={open}
            onClose={() => setAnchorEl(null)}
          >
            <MenuItem onClick={() => deleteList(true)}>Delete List</MenuItem>
          </Menu>

          <Cards listId={listId} />
        </ListCard>
      )}
    </>
  );
}

export default ListElement;

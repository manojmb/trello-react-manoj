import React, { useState } from "react";
import { Box, Button, Modal, Typography } from "@mui/material";

import CheckList from "./CheckList";

import { modalStyle } from "../styles/MaterialStyles";
import { deleteCardById } from "../api/API";
import ToastError from "./ToastError";

function CardModal({ card, setCardData }) {
  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");

  const deleteCard = async (cardId) => {
    try {
      await deleteCardById(cardId);
      setCardData((prevData) => prevData.filter((card) => card.id != cardId));
    } catch (error) {
      setError(error.message);
    }
    setOpen(false);
  };
  return (
    <>
      {error && <ToastError message={error} />}
      <p
        onClick={() => setOpen(true)}
        style={{ width: "100%", margin: 0, padding: "0.5rem 1rem" }}
      >
        {card.name}
      </p>
      <Modal
        sx={{ overflowX: "auto" }}
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            Card Name : {card.name}
            <Typography id="modal-modal-title" variant="h6" component="h2">
              <Button variant="contained" onClick={() => deleteCard(card.id)}>
                Delete
              </Button>
            </Typography>
          </div>
          <CheckList cardId={card.id} />
        </Box>
      </Modal>
    </>
  );
}

export default CardModal;

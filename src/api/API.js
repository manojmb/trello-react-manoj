import axios from "axios";

const key = import.meta.env.VITE_KEY;
const token = import.meta.env.VITE_TOKEN;
const baseUrl = "https://api.trello.com/1/";

axios.defaults.params = {
  key: key,
  token: token,
};

//Board API
export const getAllBoards = async () => {
  const response = await axios.get(
    `${baseUrl}members/me/boards?fields=name,url&filter=open`
  );
  return response;
};

export const createBoardByName = async (boardName) => {
  let response = await axios.post(`${baseUrl}boards/?name=${boardName}`);

  return response;
};

export const deleteBoardById = async (boardId) => {
  const response = await axios.delete(`${baseUrl}boards/${boardId}`);
  return response;
};

// List API
export const getAllLists = async (boardId) => {
  const response = await axios.get(`${baseUrl}boards/${boardId}/lists`);
  return response;
};

export const createList = async (boardId, listName) => {
  const response = await axios.post(
    `${baseUrl}lists?name=${listName}&idBoard=${boardId}`
  );

  return response;
};

export const deleteListById = async (listId, value) => {
  const response = await axios.put(
    `${baseUrl}lists/${listId}/closed?value=${value}`
  );

  return response;
};

// Card API
export const getAllCards = async (listId) => {
  const response = await axios.get(`${baseUrl}lists/${listId}/cards`);
  return response;
};

export const createCard = async (listId, cardName) => {
  const response = await axios.post(
    `${baseUrl}cards?idList=${listId}&name=${cardName}`
  );

  return response;
};

export const deleteCardById = async (cardId) => {
  const response = await axios.delete(`${baseUrl}cards/${cardId}`);
  return response;
};

// CheckList API
export const getCheckList = async (cardId) => {
  const response = await axios.get(`${baseUrl}cards/${cardId}/checklists`);
  return response;
};
export const createCheckListById = async (cardId, name) => {
  const response = await axios.post(
    `${baseUrl}checklists?name=${name}&idCard=${cardId}`
  );
  return response;
};
export const deleteCheckListById = async (checkListId) => {
  const response = await axios.delete(`${baseUrl}checklists/${checkListId}`);
  return response;
};

// CheckItem API
export const getCheckItems = async (checkListId) => {
  const response = await axios.get(
    `${baseUrl}checklists/${checkListId}/checkItems`
  );
  return response;
};

export const createCheckItems = async (checkListId, name) => {
  const response = await axios.post(
    `${baseUrl}checklists/${checkListId}/checkItems?name=${name}`
  );
  return response;
};

export const deleteCheckItemById = async (checkListId, checkItemId) => {
  const response = await axios.delete(
    `${baseUrl}checklists/${checkListId}/checkItems/${checkItemId}`
  );
  return response;
};

export const strikeCheckItems = async (cardId, checkItemId, value) => {
  const response = await axios.put(
    `${baseUrl}cards/${cardId}/checkItem/${checkItemId}?state=${value}`
  );
  return response;
};

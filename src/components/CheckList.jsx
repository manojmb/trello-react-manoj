import React, { useState } from "react";
import { Button, List } from "@mui/material";

import {
  deleteCheckListById,
  getCheckList,
  createCheckListById,
} from "../api/API";
import CheckItems from "./CheckItems";
import CircularLoading from "./CircularLoading";
import AddItemWithPopover from "./AddItemWithPopover";
import useApi from "../hooks/useApi";
import ToastError from "./ToastError";

function CheckList({ cardId }) {
  const {
    data: checkLists,
    setData: setCheckLists,
    error,
    isLoading,
  } = useApi(getCheckList, cardId);
  const [toastError, setToastError] = useState("");

  async function createListById(checkListName) {
    try {
      const response = await createCheckListById(cardId, checkListName);
      setCheckLists((prevData) => [...prevData, response.data]);
    } catch (error) {
      throw error;
    }
  }

  async function deleteCheckList(checkListId) {
    try {
      await deleteCheckListById(checkListId);
      setCheckLists((prevData) =>
        prevData.filter((chkList) => chkList.id != checkListId)
      );
    } catch (error) {
      setToastError(error.message);
    }
  }
  return (
    <>
      {toastError && <ToastError message={toastError} />}
      {error ? <p>error</p> : null}
      {isLoading ? <CircularLoading /> : null}
      {!error && !isLoading && (
        <AddItemWithPopover
          createItem={createListById}
          title={["CheckList", "CheckList Title:-"]}
        />
      )}
      {!error && !isLoading && checkLists.length > 0 && (
        <List>
          {checkLists.map((checkList) => {
            return (
              <List key={checkList.id}>
                <List>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <img src="/checklist.svg" alt="" />
                      {checkList.name}
                    </div>
                    <Button onClick={() => deleteCheckList(checkList.id)}>
                      Delete
                    </Button>
                  </div>

                  <CheckItems checkListId={checkList.id} cardId={cardId} />
                </List>
              </List>
            );
          })}
        </List>
      )}
    </>
  );
}
export default CheckList;

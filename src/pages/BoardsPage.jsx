import React from "react";
import { getAllBoards, createBoardByName } from "../api/API";
import Board from "../components/Board";
import CircularLoading from "../components/CircularLoading";
import AddItemWithPopover from "../components/AddItemWithPopover";

import { BoardContainer, BoardCard } from "../styles/MaterialStyles";
import useApi from "../hooks/useApi";

function BoardsPage() {
  const {
    data: boards,
    setData: setBoards,
    error,
    isLoading,
  } = useApi(getAllBoards);

  async function createBoard(boardName) {
    try {
      const response = await createBoardByName(boardName);
      setBoards((prevData) => [...prevData, response.data]);
    } catch (error) {
      throw error;
    }
  }

  return (
    <>
      <h2>Your Boards</h2>
      {isLoading && <CircularLoading />}
      {error && (
        <p style={{ color: "white", textAlign: "center" }}>
          {error.message === "Network Error"
            ? error.message
            : "Error fetching boards"}
        </p>
      )}
      {!error && !isLoading && (
        <BoardContainer>
          <BoardCard
            sx={{
              backgroundColor: "#A1BDD914",
            }}
          >
            <AddItemWithPopover
              createItem={createBoard}
              title={["Create new board", "Board Title:-"]}
            />
          </BoardCard>

          {boards.map((board) => (
            <Board
              key={board.id}
              boardId={board.id}
              boardName={board.name}
              setBoards={setBoards}
            />
          ))}
        </BoardContainer>
      )}
    </>
  );
}

export default BoardsPage;

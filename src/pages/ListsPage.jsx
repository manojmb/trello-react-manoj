import React from "react";
import { useParams } from "react-router-dom";

import CircularLoading from "../components/CircularLoading";
import AddItemWithForm from "../components/AddItemWithForm";
import ListElement from "../components/ListElement";
import { getAllLists, createList } from "../api/API";
import { AddListButtonStyle } from "../styles/MaterialStyles";
import { Add as AddIcon, Close as CloseIcon } from "@mui/icons-material";
import useApi from "../hooks/useApi";

import {
  ListContainer,
  OrderedList,
  OrderedListItem,
} from "../styles/MaterialStyles";
import ToastError from "../components/ToastError";

const list = [
  AddListButtonStyle,
  <>
    <AddIcon style={{ fill: "#FFF" }} />
    Add another list
  </>,
  "Enter list title...",
  <CloseIcon style={{ fill: "#fff" }} />,
];
function ListsPage() {
  const { board_id } = useParams();
  const {
    data: lists,
    setData: setLists,
    error,
    isLoading,
  } = useApi(getAllLists, board_id);

  async function createListsByName(itemName) {
    try {
      const response = await createList(board_id, itemName);
      setLists((prevData) => [...prevData, response.data]);
    } catch (error) {
      throw error;
    }
  }

  return (
    <>
      <h2>Lists</h2>
      {error && (
        <p style={{ color: "white", textAlign: "center" }}>
          {error.message === "Network Error" ? error.message : "No such board"}
        </p>
      )}
      {isLoading && <CircularLoading />}
      {!error && !isLoading && (
        <ListContainer>
          <OrderedList>
            {lists.map((list) => (
              <OrderedListItem key={list.id}>
                <ListElement
                  listId={list.id}
                  listName={list.name}
                  setLists={setLists}
                />
              </OrderedListItem>
            ))}
            <OrderedListItem>
              <div
                style={{
                  display: "flex",
                  flex: "1",
                  backgroundColor: "#101204",
                  borderRadius: "12px",
                }}
              >
                <AddItemWithForm
                  itemProperties={list}
                  createItem={createListsByName}
                />
              </div>
            </OrderedListItem>
          </OrderedList>
        </ListContainer>
      )}
    </>
  );
}

export default ListsPage;

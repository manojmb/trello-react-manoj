import React from "react";

function ErrorPage() {
  return (
    <>
      <div class="error-page" style={{ textAlign: "center", color: "white" }}>
        <h1>Oops!</h1>
        <p>
          Sorry, an unexpected error has occurred. Please provide proper route
          or refresh
        </p>
      </div>
    </>
  );
}

export default ErrorPage;

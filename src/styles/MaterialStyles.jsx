import {
  Container,
  List,
  ListItem,
  Card,
  CardHeader,
  TextField,
  CardContent,
  InputBase,
} from "@mui/material";
import { styled, alpha } from "@mui/material/styles";

export const StyledPopoverContent = styled("div")({
  padding: "16px",
  display: "flex",
  flexDirection: "column",
  color: "white",
  backgroundColor: "rgb(50,57,64)",
});

export const Buttons = styled("div")({
  padding: "16px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const StyledTextField = styled(TextField)({
  resize: "none",
  backgroundColor: "#22272B",
  "& input::placeholder": {
    color: "white",
  },
  "& input": {
    color: "white",
    border: "none",
  },
  color: "white",
  borderRadius: "12px",
  maxHeight: "50px",
});

export const AddCardButtonStyle = {
  display: "flex",
  flex: 1,
  borderRadius: "12px",
  backgroundColor: "transparent",
  gap: "0.5rem",
  justifyContent: "flex-start",
  textTransform: "none",
  margin: "0 0.5rem",
  padding: "0.5rem 1rem",
  color: "white",
  "&:hover": {
    backgroundColor: "#A6C5E229",
  },
};
export const AddListButtonStyle = {
  display: "flex",
  flex: 1,
  borderRadius: "12px",
  gap: "0.5rem",
  justifyContent: "flex-start",
  textTransform: "none",
  backgroundColor: "#1B6795",
  padding: "1rem",
  color: "white",
  "&:hover": {
    backgroundColor: "#A6C5E229",
  },
};

export const AddCardForm = styled("form")({
  display: "flex",
  flex: "1",
  flexDirection: "column",
  gap: "0.5rem",
  padding: "0.5rem",
});

export const BoardContainer = styled(Container)(({ theme }) => ({
  display: "flex",
  flex: 1,
  maxWidth: "initial !important",
  width: "100%",
  gap: "1rem",
  padding: "12px",
  marginTop: theme.spacing(3),
  minHeight: "120px",
  marginLeft: 0,
  marginRight: 0,
  flexWrap: "wrap",
}));

export const BoardCard = styled(Card)({
  width: "300px",
  // "-webkit-flex": "initial",
  position: "relative",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  minHeight: "120px",
  justifyContent: "center",
});

export const CardHeaderContainer = styled(CardHeader)(({ theme }) => ({
  backgroundColor: "transparent",
  padding: 0,
  position: "absolute",
  top: 0,
  right: 0,
  margin: theme.spacing(1),
}));

export const ListCardContent = styled(Container)({
  padding: "0 !important",
  color: "white",
  fontSize: "14px",
  backgroundColor: "#22272B",
  borderRadius: "8px",
});

export const CardContainer = styled(CardContent)({
  display: "flex",
  flexDirection: "column",
  gap: "0.5rem",
  padding: "1rem 1rem 0rem 1rem",
});

export const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),

  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

export const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

export const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(2, 2, 2, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export const modalStyle = {
  // display: "flex",
  flexDirection: "row",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  color: "#B6C2CF",
  outline: "none",
  backgroundColor: "rgb(50,57,64)",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  maxHeight: "500px",
  overflow: "scroll",
};

export const ListContainer = styled(Container)(({ theme }) => ({
  display: "flex",
  padding: "1rem",
  maxWidth: "initial !important",
  marginTop: theme.spacing(3),
  minHeight: "120px",
  marginLeft: 0,
  overflowY: "auto",
}));

export const ListCard = styled(Card)({
  padding: "0",
  backgroundColor: "#101204",
  borderRadius: "0.5rem",
  color: "white",
  cursor: "pointer",
  flex: "1",
});

export const ListHeader = styled(CardHeader)({
  padding: "0.5rem 1rem",
  color: "#B6C2CF",
  fontSize: "14px",
  fontWeight: "600",
});

export const OrderedList = styled(List)({
  display: "flex",
  flex: "1",
  alignItems: "flex-start",
});
export const OrderedListItem = styled(ListItem)({
  flex: "1",
  width: "284px",

  maxWidth: "284px",
});

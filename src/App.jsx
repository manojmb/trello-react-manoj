import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

import BoardsPage from "./pages/BoardsPage";
import ListsPage from "./pages/ListsPage";
import Header from "./components/Header";
import ErrorPage from "./pages/ErrorPage";


function App() {
  return (
    <Router>
      <Header />
      <main>
        <Routes>
          {/* Redirect from "/" to "/boards" */}
          <Route path="/" element={<Navigate to="/boards" replace />} />

          {/* Route for "/boards" */}
          <Route path="/boards" exact element={<BoardsPage />} />

          {/* Route for "/boards/:board_id" */}
          <Route path="/boards/:board_id" element={<ListsPage />} />

          {/* Catch-all route for non-existent routes */}
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </main>
    </Router>
  );
}

export default App;

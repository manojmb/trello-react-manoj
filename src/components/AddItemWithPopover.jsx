import React, { useState } from "react";
import { Button, Popover, Typography } from "@mui/material";

import CircularLoading from "./CircularLoading";
import ToastError from "./ToastError";

import {
  StyledPopoverContent,
  Buttons,
  StyledTextField,
} from "../styles/MaterialStyles";

const AddItemWithPopover = ({ createItem, title }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const [inputName, setInputName] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  async function addItem() {
    setIsLoading(true);
    try {
      createItem(inputName);
      setError("");
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }

    setInputName("");
    setAnchorEl(null);
  }

  return (
    <>
      {error && <ToastError message={error} />}
      {isLoading && <CircularLoading />}
      {!isLoading && (
        <div>
          <Button
            aria-controls="simple-menu"
            aria-haspopup="true"
            variant="outlined"
            onClick={(event) => setAnchorEl(event.currentTarget)}
            sx={{
              color: "#B6C2CF",
              backgroundColor: "#45505B",
              border: "none",
            }}
          >
            {title[0]}
          </Button>
          <Popover
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={() => setAnchorEl(null)}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
          >
            <StyledPopoverContent>
              <Typography style={{ padding: "1rem 0.5rem" }}>
                {title[1]}
              </Typography>
              <StyledTextField
                variant="outlined"
                value={inputName}
                onChange={(event) => setInputName(event.target.value)}
              />
              <Buttons>
                <Button color="primary" onClick={addItem}>
                  Add
                </Button>
                <Button
                  color="secondary"
                  onClick={() => {
                    setInputName("");
                    setAnchorEl(null);
                  }}
                >
                  Cancel
                </Button>
              </Buttons>
            </StyledPopoverContent>
          </Popover>
        </div>
      )}
    </>
  );
};

export default AddItemWithPopover;
